<?php
require ("../db/ModelBase.php");
require '../db/IModel.php';

class Usuario extends ModelBase implements IModel{

	private $id;
	private $nick;
	private $password;
	private $nombre;
	private $apellido1;
	private $apellido2;
	private $celular;
	private $tipousuario_id;
	private $email;


	function __construct() {
		parent::__construct(get_class($this));
	}

	public function save() {
		// Obtiene los valores y campos de la clase
		$values = get_object_vars($this);
		$this->id = parent::save($values);
	}

	public function getData($id) {
		$fields = $this->getFields();
		$data = parent::get($fields, ['id' => $id]);

		if ($data && count($data) > 0) {
			$data = $data[0];
			foreach ($data as $key => $value) {
				$this->$key = $value;
			}
		}
		return $data;
	}

	public function login($email, $password)
	{
		$passEncrypt = md5($password . 'UNIR');
		$fields = $this->getFields();
		$where = ['email' => $email, 'password' => $passEncrypt];
		$data = parent::get($fields, $where);
		isset($data['id']);

		if (count($data) > 0) {
			return $data;
		} else {
			return false;
		}
	}

	public function getAll() {
		$fields = $this->getFields();
		$data = parent::get($fields);
		return $data;
	}

	public function update() {
		$values = get_object_vars($this);
		parent::update($values, ['id' => $this->id]);
	}

	public function setData($nick, $apellido1, $apellido2 = "", $celular, $email) {
		$this->nick = $nick;
		$this->apellido1 = $apellido1;
		$this->apellido2 = $apellido2;
		$this->celular = $celular;
		$this->email = $email;
		$this->tipousuario_id = 1;
	}

	/**
	 * @uses
	 * */
	public function registroSimple($email, $nombre, $apellido1, $password){
		// Por seguridad encripto el pass
		$passEncrypt = md5($password . 'UNIR');
		$this->email = $email;
		$this->nombre = $nombre;
		$this->apellido1 = $apellido1;
		$this->password = $passEncrypt;
		$this->tipousuario_id = 1;
		$values = get_object_vars($this);
		parent::save($values);
	}

	// Obtiene todos los atributos de la clase
	private function getFields() {
		$info = get_object_vars($this);
		$fields = array_keys($info);
		return $fields;
	}

	public function delete($id) {
		parent::delete(['id' => $id]);
	}

}