<?php
	$data = new stdClass();

	$usuariosString = $_GET['usuarios'];
	$usuarios = json_decode($usuariosString);
?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Content-Language" content="es" />
	</head>
	<body>
	<form action="../controller/root.php" method="post">
		<h1 class="titulo_pagina">Registro Personal</h1>
		<div id="gallery">
		      <input type="hidden" name="controller" value="usuario">
		      <input type="hidden" name="action" value="nuevo">
			<table>
  			 	<tr>
    				<td align="justify">
						<h2 class="title1">Informaci&oacute;n  </h2>
  			 			<?php if ($_GET && key_exists('message', $_GET)) {?>
  			 				<div align="center" style=" border: 2px solid red; width: 60%;">
  			 					<?php echo $_GET['message'];?>
  			 				</div>
  			 			<?php }?>
						<div>
							<table border="2">
								<thead>
									<tr>
										<td>Email</td>
										<td>Nombre</td>
										<td>Apellidos</td>
										<td>Celular</td>
										<td>Eliminar</td>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($usuarios as $usuario) {?>
										<tr>
											<td><?php echo $usuario->email?></td>
											<td><?php echo $usuario->nombre?></td>
											<td><?php echo $usuario->apellido1 . ' ' . $usuario->apellido2?></td>
											<td><?php echo $usuario->celular?></td>
											<?php if ($usuario->tipousuario_id == 1) {?>
												<td><a href="../controller/root.php?controller=usuario&action=eliminar&id=<?php echo $usuario->id?>"> X </a></td>
											<?php } else {?>
												<td>Admin</td>
											<?php }?>
										</tr>
									<?php }?>
								</tbody>
							</table>

						</div>
    				</td>
  				</tr>
			</table>
		</div>
	</form>
	</body>
</html>
