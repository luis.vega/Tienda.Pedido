<?php
require ("../../config/slq/config_db.php");
require ("../db/DbConex.php");

class ModelBase extends Db{

	private $configDb;

	function __construct($tableName) {
		$configDb = new stdClass();
		$configDb->host = DB_HOST;
		$configDb->dbname = DB_NAME;
		$configDb->dbuser = DB_USER;
		$configDb->dbpass = DB_PASS;
		$this->configDb = $configDb;
		Db::$tbl = $tableName;
		Db::$dbh = new Db($this->configDb);
	}

	public function save($data) {
		unset($data['id']);
		Db::create($data);
	}

	public function update(array $data, array $where) {
		unset($data['id']);
		$value = Db::updateDb($data, $where);
	}

	public function get(array $fields, $wheres = []) {
		$fieldsString = implode(',', $fields);
		$where = "";
		foreach ($wheres as $key => $value) {
			if ($value) {
				$where .= "$key = '$value'|";
			}
		}
		$where = rtrim($where, '|');
		$where = str_replace('|', " and ", $where);
		return Db::read($fieldsString, $where);
	}

	public function delete(array $where) {
		Db::remove($where);
	}
}