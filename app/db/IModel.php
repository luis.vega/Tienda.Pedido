<?php

interface IModel {
	public function save();
	public function update();

}
