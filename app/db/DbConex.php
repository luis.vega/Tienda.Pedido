<?php

class Db extends \PDO
{
    public static $dbh = null;
    public static $tbl = null;
    public function __construct($config)
    {
        $host = $config->host;
        $dbname = $config->dbname;
        $dsn = "mysql:dbname={$dbname};host={$host}";
        $user = $config->dbuser;
        $password = $config->dbpass;

        try {
            parent::__construct($dsn, $user, $password, [
                \PDO::ATTR_EMULATE_PREPARES => false,
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            ]);
        } catch(\PDOException $e) {
            die(__FILE__ . " " . __LINE__ . "\n" . $e->getMessage());
        }
    }

    public static function create(array $array)
    {
        $fields = $values = '';
        foreach($array as $key => $value) {
            $fields .= "
                `$key`,";
            $values .= "
                '$value',";
        }
        $fields = rtrim($fields, ',');
        $values = rtrim($values, ',');
        $sql = "
		 INSERT INTO `" . self::$tbl . "` ($fields)
		 VALUES ($values)";
        try {
            $stm = self::$dbh->prepare($sql);
            $stm->execute();
            return self::$dbh->lastInsertId();
        } catch(\PDOException $e) {
            die(__FILE__ . " " . __LINE__ . "\n" . $e->getMessage());
        }
    }

    public static function updateDb(array $set, array $whereArray)
    {
    	$set_str = '';
    	foreach($set as $key=>$v) $set_str .= "
    	$key = '$v',";

    	$set_str = rtrim($set_str, ',');
    	$where = "";
    	foreach ($whereArray as $key => $value) {
    		if ($value) {
    			$where .= "$key = '$value',";
    		}
    	}
    	$where = rtrim($where, ',');

    	$sql = "
		 UPDATE `" . self::$tbl . "` SET$set_str
		     WHERE $where";
    	try {
    		$stm = self::$dbh->prepare($sql);
    		return $stm->execute();
    	} catch(\PDOException $e) {
    		die(__FILE__ . " " . __LINE__ . "\n" . $e->getMessage());
    	}
    }

    public static function read(
    		$field,
    		$where = '',
    		$wval  = '',
    		$extra = '',
    		$type  = 'all')
    {
    	$w = $where ? "
    	WHERE $where " : '';
    	$a = $wval ? ['wval' => $wval] : [];
    	$sql = "
    	SELECT $field
    	FROM `" . self::$tbl . "`$w $extra";

		try {
			if ($type !== 'all') $sql .= ' LIMIT 1';
			$stm = self::$dbh->prepare($sql);
			if ($stm->execute()) {
				if ($type === 'all') $res = $stm->fetchAll();
				elseif ($type === 'one') $res = $stm->fetch();
				elseif ($type === 'col') $res = $stm->fetchColumn();
				$stm->closeCursor();
				return $res;
			} else return false;
		} catch(\PDOException $e) {
			die(__FILE__ . " " . __LINE__ . "\n" . $e->getMessage());
		}
    }

    public static function remove(array $whereArray)
    {

    	$whereString = "";
    	foreach ($whereArray as $key => $value) {
    		if ($value) {
    			$whereString .= "$key = '$value',";
    		}
    	}
    	$whereString = rtrim($whereString, ',');
    	$sql = "
		 DELETE FROM `" . self::$tbl . "`
		     WHERE $whereString";
    	try {
    		if ($whereString) {
    			$stm = self::$dbh->prepare($sql);
    			return $stm->execute();
    		}
    	} catch(\PDOException $e) {
    		die(__FILE__." ".__LINE__."\n".$e->getMessage());
    	}
    }

}