<?php
session_start();
 require ("../model/Usuario.php");

$controller = $_REQUEST['controller'];
$action = $_REQUEST['action'];

if ($controller == 'usuario') {

	//Validaciones ya preparado de datos para guardar
	// Guardado de datos
	$usuarioModel = new Usuario();

	switch ($action) {
		case 'nuevo':
			$mensage = '';
			if ($_POST['password'] == $_POST['password2']) {
				$mensage = 'Inicie Session';
				$url = 'message=' .$mensage;
				$usuarioModel->registroSimple($_POST['email'], $_POST['nombre'], $_POST['apellido1'], $_POST['password']);
				header("Location: ../view/login.php?" . $url);
			} else {

				if ($_POST['password'] != $_POST['password2']) {
					$mensage = 'Las contrasenias no son iguales';
					$_POST['message'] = $mensage;
				}
				$url = 'message=' .$mensage;
				$url .= '&email=' . $_POST['email'];
				$url .= '&nombre=' . $_POST['nombre'];
				$url .= '&apellido1=' . $_POST['apellido1'];
				header("Location: ../view/registro.php?" . $url);
				die();
			}
		;
		break;

		case 'login':
			$datos = $usuarioModel->login($_POST['email'], $_POST['password']);
			if ($datos !== false) {

				$_SESSION['cliente'] = $datos;
				header("Location: ../view/principal.html");
			} else {
				$mensage = 'Usuario o Password no son correctos';
				header("Location: ../view/login.php?message=". $mensage . '&email='. $_POST['email']);
			}
			;
		break;

		case 'listar':
			if ($_SESSION && array_key_exists('cliente', $_SESSION)) {
				$info = $_SESSION['cliente'][0];
				if ($info['tipousuario_id'] == 2) {
					$datos = $usuarioModel->getAll();
					header("Location: ../view/listaUsuarios.php?usuarios=". json_encode($datos));
				} else {
					$mensage = 'No tiene Permisos de Administrador';
					header("Location: ../view/login.php?message=" . $mensage);
				}
			} else {
				$mensage = 'Inicie Sesion';
				header("Location: ../view/login.php?message=" . $mensage);
			}
		break;

		case 'eliminar':
			if ($_SESSION && array_key_exists('cliente', $_SESSION)) {
				$info = $_SESSION['cliente'][0];
				if ($info['tipousuario_id'] == 2 && $_GET && key_exists('id', $_GET)) {
					$usuarioModel->delete($_GET['id']);
					$datos = $usuarioModel->getAll();
					header("Location: ../view/listaUsuarios.php?usuarios=". json_encode($datos));
				} else {
					$mensage = 'No tiene Permisos de Administrador';
					header("Location: ../view/login.php?message=" . $mensage);
				}
			} else {
				$mensage = 'Inicie Sesion';
				header("Location: ../view/login.php?message=" . $mensage);
			}
		break;
		default:
			;
		break;
	}
}
