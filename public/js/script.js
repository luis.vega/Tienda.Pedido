// función que carga el menu izquierdo y añade la clase "over".
startList = function() {
	navRoot = document.getElementById("nav");
	for (i=0; i<navRoot.childNodes.length; i++) {
		node = navRoot.childNodes[i];
		if (node.nodeName == "LI") {
			node.onmouseover = function() {
				this.className += " over";
			}
			node.onmouseout = function() {
				this.className = this.className.replace(" over", "");
			}
		}
	}
}

// función que se usa en la página index.html para cargar las páginas de los artículos dentro del frame.
function CargarArticulo(elem){
	var iframe = document.getElementById("contenido");
	iframe.src = elem.href;
}

// función que indica los autores accediendo al DOM, busca un elemento por id, crea un hijo e indica el texto con los autores.
function SetAutores(){			
	document.getElementById("autores");
	document.getElementById("autores").appendChild;
	document.getElementById("autores").childNodes[0].textContent = " Jos\u00e9 Manuel Gonzalez, Luis A. Vega, Alejandro, Roberto";
}

// Llama a las funciones indicadas tras la carga de la pagina index.html.
function OnLoadWindow(){			
	SetAutores();
	startList;
}

window.onload = OnLoadWindow;