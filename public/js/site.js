$('[data-equal-id]').bind('input', function() {
    var to_confirm = $(this);
    var to_equal = $('#' + to_confirm.data('equalId'));

    if(to_confirm.val() != to_equal.val())
        this.setCustomValidity('Password must be equal');
    else
        this.setCustomValidity('');
});