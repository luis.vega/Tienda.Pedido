-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Versi�n del servidor: 5.6.16
-- Versi�n de PHP: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


--
-- Base de datos: `Servicio`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva`
--

CREATE TABLE IF NOT EXISTS `reserva` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_inicio` datetime NOT NULL,
  `fecha_fin` datetime NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `usuario_id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `servicio_id` int(11) NOT NULL,
  PRIMARY KEY (`res_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE IF NOT EXISTS `servicio` (
  `ser_id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(200) NOT NULL,
  `descripcion` text NOT NULL,
  `estado` enum('pendiente', 'iniciado','anulado') DEFAULT NULL,
  `image_name` text NOT NULL,
  PRIMARY KEY (`ser_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `tipousuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `tipousuario`
--

INSERT INTO `tipousuario` (`id`, `nombre`, `descripcion`) VALUES
(1, 'administrador', 'Usuario con privilegios de permisos sobre el sistema y la administracion de acceso, cuentas, reservas, etc.'),
(2, 'empleado', 'Usuario con privilegios de reservas, registro de clientes, administracion de servicios.'),
(3, 'cliente', 'Usuario con privilegios de reservas sobre servicios, administracion de la informacion de su perfil.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--


CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nick` varchar(120) NOT NULL,
  `password` varchar(200) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `apellido1` varchar(250) DEFAULT NULL,
  `apellido2` varchar(250) DEFAULT NULL,
  `celular` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `tipousuario_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
-- Usuario Admin para que pueda ver la lista de usuarios.
-- User: admin@unir.net, pass:1234
INSERT INTO `tienda`.`usuario` (`id`, `nick`, `password`, `nombre`, `apellido1`, `apellido2`, `celular`, `email`, `tipousuario_id`) VALUES (NULL, 'Profesora', '99c39aac25538e211988ed83bd685e22', 'Natalia', 'Unir', '', '', 'admin@unir.net', '2');
