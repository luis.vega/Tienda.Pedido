<!DOCTYPE html>
<html>
  <head>
    <meta charset="ISO-8859-1">
    <title>Tienda de Tecnolog�a</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" href="public/css/style.css"> <!-- Ruta al .css usado -->
  </head>

  <body>
    <div id="container">
      <div id="encabezamiento"> <!-- Encabezamiento de la pagina principal-->
	    <div id="encabezamiento_texto" style="padding: 10px;"> <!-- Establece la anchura de todas las zonas de relleno de los elementos-->
		  <h1><a>Tienda de Tecnologia</a></h1> <!-- No se hace link a nada -->
		  <!-- Mostramos una etiqueta y los autores del trabajo  -->
		  <span class="autores_label">Web realizada por: <span id="autores" class="autores"> </span></span>
		</div>
		<!-- Imagen de la organizaci�n  -->
      	<img src="public/img/Encabezado/banner4.jpg" width="100%" height="100px" />
  	  </div>
      <div id="izquierda">
	    <h2 class="titulo_categoria under">Categor&iacutea </h2>
		<section class="seccion" align="top"> <!-- contiene lista para el acceso directo a los distintos elementos de la tienda  -->
			<ul id="nav" class="menu">
			  <li><a href="app/view/principal.html" onclick="CargarArticulo(this); return false;">Inicio</a></li>
			  <li><a href="#">Servidores</a> <!--acceso o enlace interno para mostrar en lado derecho los servidores que est�n en la categor�a-->
				<ul>
				  <li><a href="app/view/servidores_1.html" onclick="CargarArticulo(this); return false;"> Servidor HP BL460c</a></li><!--Evento que al hacer click sobre esta secci�n del men�, muestra la descripci�n del servidor en la p�gina que se enlaza al principio-->
				  <li><a href="app/view/servidores_2.html" onclick="CargarArticulo(this); return false;">Servidor IBM</a></li>
				  <li><a href="app/view/servidores_3.html" onclick="CargarArticulo(this); return false;">Servidor Lenovo</a></li>
				</ul>
			  </li>
			  <li><a href="#">PC's de Escritorio</a> <!--acceso o enlace interno para mostrar en lado derecho los computadores que est�n en la categor�a-->
				<ul>
				  <li><a href="app/view/escritorio_1.html" onclick="CargarArticulo(this); return false;" title="Enlace a PC DELL">PC Dell</a></li>
				  <li><a href="app/view/escritorio_2.html" onclick="CargarArticulo(this); return false;" title="Enlace a PC HP">PC HP</a></li>
				  <li><a href="app/view/escritorio_3.html" onclick="CargarArticulo(this); return false;" title="Enlace a PC LENOVO">PC Lenovo</a></li>
				</ul>
			  </li>
			  <li><a href="#">Tabletas</a> <!--acceso o enlace interno para mostrar en lado derecho las tablets que est�n en la categor�a-->
				<ul>
				  <li><a href="app/view/tablet_1.html" onclick="CargarArticulo(this); return false;" title="Enlace a tablet samsung tab 4">Tablet TAB 4</a></li>
				  <li><a href="app/view/tablet_2.html" onclick="CargarArticulo(this); return false;" title="Enlace a tablet samsung de 10.5">Tablet wifi 10.5</a></li>
				  <li><a href="app/view/tablet_3.html" onclick="CargarArticulo(this); return false;" title="Enlace a tablet Yoga">Tablet Yoga</a></li>
				</ul>
			  </li>
			  <li><a href="#">Mis Datos</a> <!--acceso o enlace interno para mostrar en lado derecho las tablets que est�n en la categor�a-->
				<ul>
				  <li><a href="app/view/registro.php" onclick="CargarArticulo(this); return false;" title="Registro Personal">Mi Registro</a></li>
				  <li><a href="app/view/login.php" onclick="CargarArticulo(this); return false;" title="Login Personal">Iniciar Sesi&oacute;n</a></li>
				  <li><a href="app/controller/root.php?controller=usuario&action=listar" onclick="CargarArticulo(this); return false;" title="Lista Usuarios">Usuarios Registrados</a></li>
				</ul>
			  </li>
			</ul>
		</section>
      </div>
      <div id="derecha"> <!--! Contiene los las imagenes de los art�culos a vender  -->
        <iframe src="app/view/login.php" class="contenido" id="contenido">
      	</iframe>
      </div>
      <footer> <!--! Pie de la p�gina  -->
        <div class="content-wrapper">
          <p>&copy; 2016 - Tienda de Tecnolog&iacuteas</p>
        </div>
      </footer>
    </div> <!--! end of #container -->

    <script defer src="public/js/script.js"></script>  <!-- ruta al fichero de scripts usados -->
    <!-- end scripts-->
  </body>
</html>